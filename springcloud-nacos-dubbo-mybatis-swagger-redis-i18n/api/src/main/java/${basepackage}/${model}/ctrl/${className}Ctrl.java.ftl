/*
* ${copyright}
*/
package ${basePackage}.${model}.ctrl;

import com.alibaba.dubbo.config.annotation.Reference;
import ${basePackage}.account.entity.Account;
import ${basePackage}.core.base.ctrl.BaseCtrl;
import ${basePackage}.core.entity.BeanRet;
import ${basePackage}.core.entity.Page;
import ${basePackage}.core.enums.ActionTypeEnum;
import ${basePackage}.core.enums.RoleTypeEnum;
import ${basePackage}.core.enums.SortColumnEnum;
import ${basePackage}.core.enums.SqlSortEnum;
import ${basePackage}.core.exceptions.BaseException;
import ${basePackage}.${model}.entity.${className};
import ${basePackage}.${model}.service.${className}SV;
import ${basePackage}.syslog.annotation.SystemControllerLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * ${notes} 控制器
 *
 * @author ${author}
 */
@Slf4j
@RestController
@RequestMapping("/${className?uncap_first}")
@Api(tags = "${className}Ctrl", description = "${notes}控制器")
public class ${className}Ctrl extends BaseCtrl {

    @Reference(version = "${r'${dubbo.consumer.version}'}")
    private ${className}SV ${classNameLower}SV;


<#if (pkFields?size>0)>
   /**
    * 查询${notes}一个详情信息
    <#list pkFields as pkField>
    * @param ${pkField.field} ${pkField.notes}
    </#list>
    * @return BeanRet
    */
    @SystemControllerLog(actionType = ActionTypeEnum.query, roleType = RoleTypeEnum.Admin, description = "查询${notes}一个详情信息")
    @ApiOperation(value = "查询${notes}一个详情信息", notes = "")
    @ApiImplicitParams({
    <#list pkFields as pkField>
        @ApiImplicitParam(name = "${pkField.field}", value = "${pkField.notes}",dataType = "${pkField.fieldType}", paramType = "query", required = true)<#if pkField_has_next>,</#if>
    </#list>
    })
    @GetMapping(value = "/load")
    @ResponseBody
    public BeanRet load(<#list pkFields as pkField>${pkField.fieldType} ${pkField.field}<#if pkField_has_next>,</#if></#list>) {
    <#list pkFields as pkField>
        if(${pkField.field}==null){
          return null;
        }
    </#list>
    ${className} ${classNameLower} = ${classNameLower}SV.load(<#list pkFields as pkField>${pkField.field}<#if pkField_has_next>,</#if></#list>);
        return BeanRet.success(${classNameLower});
    }

</#if>

    /**
    * 查询${notes}信息集合
    *
    * @return 分页对象
    */
    @SystemControllerLog(actionType = ActionTypeEnum.query, roleType = RoleTypeEnum.Admin, description = "查询${notes}信息集合")
    @ApiOperation(value = "查询${notes}信息集合", notes = "")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "curPage", value = "当前页", required = true, paramType = "query", defaultValue = "1"),
        @ApiImplicitParam(name = "pageSize", value = "分页大小", required = true, paramType = "query", defaultValue = "10"),
        @ApiImplicitParam(name = "sortColumn", value = "排序列 [枚举编号：1018](/resources/enum/1018)", dataTypeClass = SortColumnEnum.class, paramType = "query", defaultValue = "ID"),
        @ApiImplicitParam(name = "sort", value = "排序类型 [枚举编号：1014](/resources/enum/1014)", dataTypeClass = SqlSortEnum.class, paramType = "query", defaultValue = "desc"),
    })
    @GetMapping(value = "/list")
    @ResponseBody
    public BeanRet list(@ApiIgnore ${className} ${classNameLower}, SortColumnEnum sortColumn, SqlSortEnum sort,@ApiIgnore Page<${className}> page) {
        if(page==null){
            throw new BaseException(BaseException.ExceptionEnums.param_is_null, "分页对象");
        }
        List<${className}> ${classNameLower}s = ${classNameLower}SV.list(${classNameLower},page.genRowStart(),page.getPageSize());
        int total = ${classNameLower}SV.count(${classNameLower});
        page.setTotalRow(total);
        page.setVoList(${classNameLower}s);
        return BeanRet.success(page);
    }

    /**
    * 创建${notes}
    *
    * @return BeanRet
    */
    @SystemControllerLog(actionType = ActionTypeEnum.add, roleType = RoleTypeEnum.Admin, description = "创建${notes}")
    @ApiOperation(value = "创建${notes}", notes = "")
    @ApiImplicitParams({
        <#list fields as field>
                @ApiImplicitParam(name = "${field.field}", value = "${field.notes}", paramType = "query")<#if field_has_next>,</#if>
        </#list>
    })
    @PostMapping("/add")
    @ResponseBody
    public BeanRet add(@ApiIgnore ${className} ${classNameLower}) {
        Account account = JWTTools.decodeTokenToAccount(request);
        ${classNameLower}SV.add(${classNameLower}, account);
        return BeanRet.success(${classNameLower});
    }

    /**
    * 删除${notes}
    *
    * @return BeanRet
    */
    @SystemControllerLog(actionType = ActionTypeEnum.del, roleType = RoleTypeEnum.Admin, description = "删除${notes}")
    @ApiOperation(value = "删除${notes}", notes = "")
    @ApiImplicitParams({
<#list pkFields as pkField>
        @ApiImplicitParam(name = "${pkField.field}", value = "${pkField.notes}", paramType = "query", required = true)<#if pkField_has_next>,</#if>
</#list>
    })
    @DeleteMapping("/del")
    @ResponseBody
    public BeanRet delete(<#list pkFields as pkField>${pkField.fieldType} ${pkField.field}<#if pkField_has_next>,</#if></#list>) {
        ${classNameLower}SV.delete(<#list pkFields as pkField>${pkField.field}<#if pkField_has_next>,</#if></#list>);
        return BeanRet.success();
    }
}