/*
 * ${copyright}
 */
package ${basePackage}.${model}.dao;

import ${basePackage}.core.base.BaseDAO;
import ${basePackage}.${model}.entity.${className};

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * ${notes}
 *
 * @author ${author}
 */
@Mapper
@Repository
public  interface ${className}DAO extends BaseDAO<${className}, Long> {

<#if (pkFields?size>0)>

    /**
     * 删除对象${className}
     *@param params 实体的属性
     */
    void delete(Map<String, Object> params);

</#if>

<#if oneToOneList??&&(oneToOneList?size>0) || oneToManyList??&&(oneToManyList?size>0)>
    /**
     * 加载一个对象${className},所有关联数据都将被查询
     <#list pkFields as field>
     * @param ${field.field} ${field.notes}
     </#list>
     * @return ${className}
     */
    ${className} getDetail(<#list pkFields as field>@Param("${field.field}") ${field.fieldType} ${field.field}<#if field_has_next>,</#if></#list>);
</#if>

    /**
     * 查询${className}列表
     * @param rowBounds 分页参数
     * @return List<${className}>
     */
    List<${className}> list(RowBounds rowBounds);
}
