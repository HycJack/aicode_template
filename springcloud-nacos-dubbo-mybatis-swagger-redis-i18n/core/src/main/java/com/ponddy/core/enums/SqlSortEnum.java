package com.ponddy.core.enums;

/**
 * Copyright 2019 @http://www.rzhkj.com
 *
 * @Author borong
 * @Date 2019/9/20 20:33
 * @Description: SQL排序类型
 * [枚举编号：1014](/resources/enum/1014)
 */
public enum SqlSortEnum {
    // 正序排列
    asc,
    // 倒序排列
    desc;

    public boolean get() {
        return asc.equals(this);
    }
}
