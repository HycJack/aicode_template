package com.ponddy.core.enums;

/**
 * Copyright 2020 @http://www.rzhkj.com
 *
 * @Author borong
 * @Date 2020/4/15 10:45
 * @Description: 支持排序列
 * [枚举编号：1018](/resources/enum/1018)
 */
public enum SortColumnEnum {
    /**
     * 支持排序列
     */
    ID("id"),
    EDIT_TIME("update_time"),
    ;

    public String column;

    SortColumnEnum(String column) {
        this.column = column;
    }
}