package com.ponddy.core.enums;

import ${basePackage}.core.tools.DateTools;

import java.util.Date;

/**
 * Copyright 2019 @http://www.rzhkj.com
 *
 * @Author borong
 * @Date 2019/9/21 18:17
 * @Description: Sql时间类型区间查询枚举
 * [枚举编号：1015](/resources/enum/1015)
 */
public enum SqlDateQueryEnum {
    // 格式：类型
    today("今天"),
    yesterday("昨天"),
    last_week("上周"),
    last_month("上月"),
    ;
    /**
     * 描述
     */
    private String description;

    SqlDateQueryEnum(String description) {
        this.description = description;
    }

    /**
     * 获得开始时间戳（10位数字）
     * @param date
     * @return
     */
    public long getBegin(Date date){

        if (today.equals(this)) {
            return getTimestamp10(DateTools.getDayBegin(date).getTime());
        }
        else if (yesterday.equals(this)) {
            Date yesterday = DateTools.getSomeOneDate(date, -1, DateTools.DateType.day);
            return getTimestamp10(DateTools.getDayBegin(yesterday).getTime());
        }
        else if (last_week.equals(this)) {
            return getTimestamp10(DateTools.getLastWeekBegin(date).getTime());
        }
        else if (last_month.equals(this)) {
            return getTimestamp10(DateTools.getLastMonthBegin(date).getTime());
        }
        return 0;
    }

    /**
     * 获得截止时间戳（10位数字）
     * @param date
     * @return
     */
    public long getEnd(Date date){
        if (today.equals(this)) {
            return getTimestamp10(DateTools.getDayEnd(date).getTime());
        }
        else if (yesterday.equals(this)) {
            Date yesterday = DateTools.getSomeOneDate(date, -1, DateTools.DateType.day);
            return getTimestamp10(DateTools.getDayEnd(yesterday).getTime());
        }
        else if (last_week.equals(this)) {
            return getTimestamp10(DateTools.getLastWeekEnd(date).getTime());
        }
        else if (last_month.equals(this)) {
            return getTimestamp10(DateTools.getLastMonthEnd(date).getTime());
        }
        return 0;
    }

    /**
     * 获取10位数字的时间戳
     * @param timestamp
     * @return
     */
    private static long getTimestamp10(Long timestamp) {
        return new Long(String.valueOf(timestamp).substring(0, 10));
    }

}
