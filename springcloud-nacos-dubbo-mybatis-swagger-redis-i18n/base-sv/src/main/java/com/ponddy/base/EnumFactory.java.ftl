/*  * Copyright (c) 2019. 郑州仁中和科技有限公司.保留所有权利. 
http://www.rzhkj.com/ 
郑州仁中和科技有限公司保留所有代码著作权.如有任何疑问请访问官方网站与我们联系. 代码只针对特定客户使用，不得在未经允许或授权的情况下对外传播扩散.恶意传播者，法律后果自行承担.  */ package ${basePackage}.base;

import com.google.common.collect.Maps;
import ${basePackage}.basic.enums.DataTypeEnum;
import ${basePackage}.basic.enums.SortColumnBasicSettingEnum;
import ${basePackage}.core.enums.*;
import ${basePackage}.label.enums.LabelTypeEnum;
import ${basePackage}.notify.enums.SortColumnTemplateEnum;
import jdk.nashorn.internal.ir.annotations.Ignore;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnumFactory {

    /**
     * 枚举管理器
     * [枚举编号：fix_number](/resources/enum/fix_number)
     */
    public enum Enums {
        //-- 通用枚举 --
        EnvEnums(1000, EnvEnums.class, "项目运行的环境"),
        YNEnum(1001, YNEnum.class, "是否"),
        SexEnum(1002, SexEnum.class, "性别"),
        RoleEnum(1003, RoleTypeEnum.class, "角色（token）类型枚举"),
        HttpCodeEnum(1004, HttpCodeEnum.class, "请求状态码及说明"),
        FormatEnum(1005, FormatEnum.class, "内容格式枚举"),
        BucketNameEnum(1006, BucketNameEnum.class, "上传文件的归类目录"),
        FeatureStateEnum(1007, EnableEnum.class, "启用状态"),
        PermissionTypeEnum(1008, PermissionTypeEnum.class, "权限类型"),
        CheckboxEnum(1009, CheckboxEnum.class, "筛选框选择状态"),
        AdminTypeEnum(1010, AdminTypeEnum.class, "管理员类型"),
        WeekEnum(1011, WeekEnum.class, "周类型"),
        MonthEnum(1012, MonthEnum.class, "月类型"),
        ActionTypeEnum(1013, ActionTypeEnum.class, "功能操作类型"),
        SqlSortEnum(1014, SqlSortEnum.class, "SQL排序类型"),
        SqlDateQueryEnum(1015, SqlDateQueryEnum.class, "Sql时间类型区间查询枚举"),
        AuthStateEnum(1016, AuthStateEnum.class, "认证状态"),
        FileTypeEnum(1017, FileTypeEnum.class, "允许上传的文件类型"),
        SortColumnEnum(1018, SortColumnEnum.class, "支持排序列"),

        //业务枚举 2000 开始

        LanguagePlatform(3001, LanguagePlatformEnum.class, "国际化语言-键-终端平台"),
        LabelTypeEnum(3002, LabelTypeEnum.class, "标签类型"),
        DataTypeEnum(3003, DataTypeEnum.class, "数据类型"),

        SortColumnBasicSettingEnum(4000, SortColumnBasicSettingEnum.class, "系统参数配置支持排序列"),
        SortColumnTemplateEnum(4001, SortColumnTemplateEnum.class, "通知模板 支持排序列"),

        ;

        public int code;
        public Class clazz;
        public String desc;

        Enums(int code, Class clazz, String desc) {
            this.code = code;
            this.clazz = clazz;
            this.desc = desc;
        }

        public static Enums getEnums(int code) {
            for (Enums enums : Enums.values()) {
                if (enums.code == code) {
                    return enums;
                }
            }
            return null;
        }

        public static List<Map<String, String>> getEnumsDescs() {
            List<Map<String, String>> list = new ArrayList<Map<String, String>>();
            for (Enums enums : Enums.values()) {
                Map<String, String> map = new HashMap<>();
                map.put("枚举编码 [code = " + String.valueOf(enums.code) + "]", enums.desc);
                list.add(map);
            }
            return list;
        }

        /**
         * 反射获取枚举键值关系
         *
         * @param code 枚举编号
         * @return
         * @throws InstantiationException
         */
        public static Map<String, String> genMap(int code) {
            try {
                Class clazz = getEnums(code).clazz;
                Map<String, String> map = Maps.newHashMap();
                if (clazz.isEnum()) {
                    Field[] fs = clazz.getDeclaredFields();
                    List<String> ignoreKeys = new ArrayList<String>();
                    for (Field field : fs) {
                        Ignore ignore = field.getAnnotation(Ignore.class);
                        if (ignore != null) {
                            ignoreKeys.add(field.getName());
                        }
                    }
                    for (Object obj : clazz.getEnumConstants()) {
                        String type = obj.toString();
                        String classType = obj.getClass().getTypeName();
                        classType = classType.substring(classType.lastIndexOf(".") + 1);
                        for (int i = 0; i < fs.length; i++) {
                            Field f = fs[i];
                            f.setAccessible(true);
                            String fType = f.getType().getTypeName();

                            if (!fType.contains(classType)) {
                                Object val = f.get(obj);
                                map.put(type, val == null ? type : val.toString());
                            }
                        }
                    }
                    //删除 忽略key
                    if (ignoreKeys != null && ignoreKeys.size() > 0) {
                        ignoreKeys.forEach(ignoreKey -> map.remove(ignoreKey));
                    }
                }
                return map;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }

    }
}
